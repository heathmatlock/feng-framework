{-# language TemplateHaskell #-}
{-# language RankNTypes #-}

module Test.Main where

import           Hedgehog
import           Hedgehog.Generic               ( hgen )
import qualified Hedgehog.Gen                  as Gen
import qualified Hedgehog.Range                as Range

--genBin :: Gen (Bin a)
genBin :: forall a . Gen (Bin a)
genBin = hgen


{-- | The following is an example of one way to test properties on a function

    What are some properties we'd like to test on our binary tree?

--}

propFoo :: Property
propFoo = property $ do
  xs <- forAll $ Gen.list (Range.linear 0 100) Gen.alpha
  reverse (reverse xs) === xs

main :: IO Bool
main = checkParallel $$(discover)
