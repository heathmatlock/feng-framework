![feng logotype](feng.png)

> A featherweight sidekick

In theory, Feng makes it a breeze to build Servant based web apps. Keep in mind though,
I'm simply playing around. It all started with wondering how far you could go with
templates and simple string substitution instead of actually building a real framework.
Turns out, you can get pretty far!

You can see a very basic example of the layout Feng creates for your project in the
example directory.

## Getting started

1. Confirm [Nix](https://nixos.org/) is on your machine

2. Install Feng

```shell
$ cd feng
$ nix-shell
$ cabal new-install feng
$ exit
```

You might want to make sure your machine doesn't have postgres running in the background
before you open a nix shell with your new project.


3. Take flight

```shell
$ feng new myapp
$ cd myapp
$ feng -h
```

## Useful links
* One day :)


## Legal

This software is available under [BSD-3](LICENSE).
