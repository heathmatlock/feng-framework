{-# LANGUAGE LambdaCase #-}

module Main where

import Data.Text (Text, pack)
import qualified Feng.Commands.Gen
import qualified Feng.Commands.Migrate
import qualified Feng.Commands.New
import System.Environment (getArgs)
import System.Exit (exitFailure)

data Command
  = New Text
  | Gen Text [Text]
  | Migrate

main :: IO ()
main = getArgs >>= \case
  "--help" : _ -> showHelp
  "-h" : _ -> showHelp
  xs -> run (parseCommand xs)

run :: Either Text Command -> IO ()
run (Left err) = putStrLn (show err) >> exitFailure
run (Right cmd) = case cmd of
  New appName -> Feng.Commands.New.run appName
  Gen generator args -> Feng.Commands.Gen.run generator args
  Migrate -> Feng.Commands.Migrate.run

parseCommand :: [String] -> Either Text Command
parseCommand = \case
  "new" : appName : _ -> Right $ New (pack appName)
  "gen" : generator : args -> Right $ Gen (pack generator) (fmap pack args)
  "migrate" : _ -> Right $ Migrate
  _ -> Left "Feng Error: either command not recognized or not used correctly."

showHelp :: IO ()
showHelp = do
  putStrLn "== Available Feng commands =="
  putStrLn ""
  putStrLn "Command:"
  putStrLn "    new APP_NAME"
  putStrLn ""
  putStrLn "Description:"
  putStrLn "    Creates a new app with a name of your choosing."
  putStrLn ""
  putStrLn "Usage:"
  putStrLn "    $ feng new appname"
  putStrLn ""
  putStrLn "Command:"
  putStrLn "    gen [html|data]"
  putStrLn ""
  putStrLn "Description:"
  putStrLn "    Generators for various components of your app."
  putStrLn "    The data generator creates a new data layer."
  putStrLn "    The html generator creates a template, controller, and data layer for your app."
  putStrLn ""
  putStrLn "Usage:"
  putStrLn "    $ feng gen data Users User email:Text pass:Text"
  putStrLn "    $ feng gen html Products Product name:Text price:Int"
