module Feng.Commands.Migrate (run) where

import Data.Text.Encoding (encodeUtf8)
import Database.PostgreSQL.Simple
import Database.PostgreSQL.Simple.Migration
import Database.PostgreSQL.Simple.Util
import qualified Feng.Config as Feng
import System.Directory (doesDirectoryExist)

run :: IO ()
run = do
  config <- Feng.buildConfigOrExit
  let
    dbName = encodeUtf8 $ Feng.dbName config
    dbUser = encodeUtf8 $ Feng.dbUser config
  dirExists <- doesDirectoryExist "files/migrations"
  if dirExists
    then do
      conn <- connectPostgreSQL
        ("dbname=" <> dbName <> " " <> "user=" <> dbUser)
      withTransaction conn $ do
        result <- runMigration
          $ MigrationContext (MigrationDirectory "files/migrations") True conn
        pure ()
    else do
      putStrLn "Feng Error: could not find files/migrations"
      putStrLn ""
      putStrLn "Are you in the root directory of the project?"
      putStrLn ""
