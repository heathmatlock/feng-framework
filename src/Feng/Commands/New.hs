module Feng.Commands.New
  ( run
  )
where

import qualified Data.ByteString as B
import Data.Foldable (traverse_)
import Data.Text (unpack)
import qualified Data.Text as T
import Feng.Prelude (CaseStyle (..), cabalFilename, generateFile, swap)
import Paths_feng (getDataFileName)
import System.Directory (createDirectory, createDirectoryIfMissing)

{-
   Useful hints on working with files:
     * https://www.snoyman.com/blog/2016/12/beware-of-readfile
     * https://www.fpcomplete.com/haskell/tutorial/string-types/
-}

run :: T.Text -> IO ()
run appNameText = do
  let
    appNameString = T.unpack appNameText
    appNameStringTitle = T.unpack (T.toTitle appNameText)

  traverse_
    createDirectory
    [ appNameString
    , appNameString <> "/files"
    , appNameString <> "/files/assets"
    , appNameString <> "/files/assets/js"
    , appNameString <> "/files/assets/css"
    , appNameString <> "/files/vendor"
    , appNameString <> "/src"
    , appNameString <> "/src/" <> appNameStringTitle
    , appNameString <> "/src/" <> appNameStringTitle <> "/Data"
    , appNameString <> "/src/" <> appNameStringTitle <> "/Web"
    , appNameString <> "/src/" <> appNameStringTitle <> "/Web/Layouts"
    , appNameString <> "/src/" <> appNameStringTitle <> "/Web/Views"
    , appNameString <> "/src/" <> appNameStringTitle <> "/Web/Handlers"
    , appNameString <> "/test"
    ]

  cabalProjectTemplate <- getDataFileName "cabal.project.txt"
  generateFile cabalProjectTemplate appNameText (appNameString <> "/cabal.project")

  appCabalTemplate <- getDataFileName "app.cabal.txt"
  generateFile
    appCabalTemplate
    appNameText
    (appNameString <> "/" <> (unpack $ cabalFilename appNameText))

  setupTemplate <- getDataFileName "setup.hs.txt"
  generateFile setupTemplate appNameText (appNameString <> "/Setup.hs")

  nixShellTemplate <- getDataFileName "shell.nix.txt"
  generateFile nixShellTemplate appNameText (appNameString <> "/shell.nix")

  readmeTemplate <- getDataFileName "readme.md.txt"
  generateFile readmeTemplate appNameText (appNameString <> "/README.md")

  gitIgnoreTemplate <- getDataFileName "gitignore.txt"
  generateFile gitIgnoreTemplate appNameText (appNameString <> "/.gitignore")

  brittanyTemplate <- getDataFileName "brittany.yaml.txt"
  generateFile brittanyTemplate appNameText (appNameString <> "/brittany.yaml")

  stylishHaskellTemplate <- getDataFileName "stylish-haskell.yaml.txt"
  generateFile stylishHaskellTemplate appNameText (appNameString <> "/.stylish-haskell.yaml")

  mainTemplate <- getDataFileName "main.hs.txt"
  generateFile mainTemplate appNameText (appNameString <> "/src/Main.hs")

  testDriverTemplate <- getDataFileName "test-driver.hs.txt"
  generateFile testDriverTemplate appNameText (appNameString <> "/test/Driver.hs")

  appConfigTemplate <- getDataFileName "app-config.hs.txt"
  generateFile
    appConfigTemplate
    appNameText
    (appNameString <> "/src/" <> appNameStringTitle <> "/Config.hs")

  appDataHomeTemplate <- getDataFileName "app-data-home.hs.txt"
  generateFile
    appDataHomeTemplate
    appNameText
    (appNameString <> "/src/" <> appNameStringTitle <> "/Data/Home.hs")

  appWebRoutesTemplate <- getDataFileName "app-web-routes.hs.txt"
  generateFile
    appWebRoutesTemplate
    appNameText
    (appNameString <> "/src/" <> appNameStringTitle <> "/Web/Routes.hs")

  appWebHandlersTemplate <- getDataFileName "app-web-handlers.hs.txt"
  generateFile
    appWebHandlersTemplate
    appNameText
    (appNameString <> "/src/" <> appNameStringTitle <> "/Web/Handlers.hs")

  appWebHandlersHomeTemplate <- getDataFileName "app-web-handlers-home.hs.txt"
  generateFile
    appWebHandlersHomeTemplate
    appNameText
    (appNameString <> "/src/" <> appNameStringTitle <> "/Web/Handlers/Home.hs")

  appWebLayoutsMainTemplate <- getDataFileName "app-web-layouts-main.hs.txt"
  generateFile
    appWebLayoutsMainTemplate
    appNameText
    (appNameString <> "/src/" <> appNameStringTitle <> "/Web/Layouts/Main.hs")

  appWebViewsHomeTemplate <- getDataFileName "app-web-views-home.hs.txt"
  generateFile
    appWebViewsHomeTemplate
    appNameText
    (appNameString <> "/src/" <> appNameStringTitle <> "/Web/Views/Home.hs")

  let
    noFormPath = appNameString <> "/src/Extra/Data/"
    noFormFile = "NoForm.hs"
    noFormFilePath = noFormPath <> noFormFile
  res <- createDirectoryIfMissing True noFormPath
  noFormTemplate <- getDataFileName "extra-data-noform.hs.txt"
  B.readFile noFormTemplate >>= B.writeFile noFormFilePath

  fengJs <- getDataFileName "feng.js"
  generateFile fengJs appNameText (appNameString <> "/files/assets/js/feng.js")

  bulma <- getDataFileName "bulma.min.css"
  generateFile bulma appNameText (appNameString <> "/files/assets/css/bulma.min.css")

  putStrLn "Almost there! Next steps:"
  putStrLn ""
  putStrLn ("    $ cd " <> appNameString)
  putStrLn "    $ nix-shell"
