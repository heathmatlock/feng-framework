module Feng.Commands.Gen (run) where

import Data.ByteString (ByteString)
import qualified Data.ByteString as B
import qualified Data.ByteString.Char8 as C
import Data.Text (Text, breakOn, toLower, toTitle, unpack)
import qualified Data.Text as T
import Data.Text.Encoding (encodeUtf8)
import Data.Time.Clock (getCurrentTime)
import Data.Time.Format (defaultTimeLocale, formatTime)
import Database.PostgreSQL.Simple (Connection, connectPostgreSQL)
import Paths_feng (getDataFileName)
import System.Directory (createDirectoryIfMissing, doesDirectoryExist)
import System.Exit (exitFailure)

import qualified Feng.Config as Feng
import Feng.Prelude
  ( addRoutesToIndex
  , addToCabalFile
  , appendCode
  , appendRouteHandlers
  , generateDataFileWithImports
  , generateHaskellFile
  , generateSqlFile
  , prependCode
  )

data Generator = Data [Text] | HTML [Text]

run :: Text -> [Text] -> IO ()
run generator args = do
  config <- Feng.buildConfigOrExit
  let
    app = Feng.appName config
    dbName = encodeUtf8 $ Feng.dbName config
    dbUser = encodeUtf8 $ Feng.dbUser config
  conn <- connectPostgreSQL ("dbname=" <> dbName <> " " <> "user=" <> dbUser)
  generate config conn app (parseGenerator generator args) False

-- TODO handle sub-paths such as:
--     $ feng gen data Blog.Posts BlogPost title:Text views:Int
parseGenerator :: Text -> [Text] -> Either Text Generator
parseGenerator generator args = case generator of
  "data" -> Right (Data args)
  "html" -> Right (HTML args)
  _ -> Left "Feng Error: generator not recognized."

generate :: Feng.Config -> Connection -> Text -> Either Text Generator -> Bool -> IO ()
generate _ _ _ (Left err) _ = putStrLn (show err) >> exitFailure
generate config conn appName (Right (Data (dataName : datumName : fields))) includeView = do
  let parsedFields = fmap parseField fields
  createDirectoryIfMissing
    True
    ("src/" <> unpack appName <> "/Data/" <> (unpack $ toTitle dataName))
  genPluralResource appName dataName datumName parsedFields
  genSingularResource appName dataName datumName parsedFields includeView
  genStatements appName dataName datumName parsedFields
  genMigrationScriptsAndLog appName dataName datumName parsedFields config
generate config conn appName (Right (HTML (dataName : datumName : fields))) _ = do
  let
    dataTitle = toTitle dataName
    dataNameLower = toLower dataName
    datumNameLower = toLower datumName
    parsedFields = fmap parseField fields
    routeIndexPath = "src/" <> unpack appName <> "/Web/Routes.hs"
    routesPath = "src/" <> unpack appName <> "/Web/Routes/" <> unpack dataTitle <> ".hs"
  createDirectoryIfMissing True ("src/" <> unpack appName <> "/Web/Routes/")
  routesTemplate <- getDataFileName "app-web-routes-plural.hs.txt"
  addToCabalFile appName (appName <> ".Web.Routes." <> dataTitle)
  -- add resource routes
  generateHaskellFile routesTemplate appName dataName datumName routesPath
  putStrLn ("Added routes in " <> routesPath)
  -- update route index
  addRoutesToIndex routeIndexPath appName dataName datumName
  putStrLn ("Updated the routes index in " <> routeIndexPath)
  -- append to App/Web/Handlers.hs
  let routeHandlerPath = "src/" <> unpack appName <> "/Web/Handlers.hs"
  appendRouteHandlers routeHandlerPath appName dataName datumName
  putStrLn ("Updated handler index at " <> routeHandlerPath)
  -- create App/Web/Handlers/Plural.hs
  let handlerPath = "src/" <> unpack appName <> "/Web/Handlers/" <> (unpack dataTitle) <> ".hs"
  putStrLn ("Creating " <> handlerPath)
  handlerTemplate <- getDataFileName "app-web-handlers-handler.hs.txt"
  generateHaskellFile handlerTemplate appName dataName datumName handlerPath
  addToCabalFile appName (appName <> ".Web.Handlers." <> dataTitle)
  -- create App/Web/Views/Plural/Index.hs
  let viewsDir = "src/" <> unpack appName <> "/Web/Views/" <> unpack dataTitle
  res <- createDirectoryIfMissing True viewsDir
  -- index view
  let indexViewPath = "src/" <> unpack appName <> "/Web/Views/" <> unpack dataTitle <> "/Index.hs"
  putStrLn ("Creating " <> indexViewPath)
  indexViewTemplate <- getDataFileName "app-web-views-plural-index.hs.txt"
  generateHaskellFile indexViewTemplate appName dataName datumName indexViewPath
  addToCabalFile appName (appName <> ".Web.Views." <> dataTitle <> ".Index")
  -- show view
  let showViewPath = "src/" <> unpack appName <> "/Web/Views/" <> unpack dataTitle <> "/Show.hs"
  putStrLn ("Creating " <> showViewPath)
  showViewTemplate <- getDataFileName "app-web-views-plural-show.hs.txt"
  generateHaskellFile showViewTemplate appName dataName datumName showViewPath
    >>= appendCode
          ("render " <> encodeUtf8 datumNameLower <> " = MainLayout.render $ do")
          [ "  H.h1 . H.text . pack . show $ "
            <> encodeUtf8 datumNameLower
            <> "Id "
            <> encodeUtf8 datumNameLower
          ]
    >>= B.writeFile showViewPath
  addToCabalFile appName (appName <> ".Web.Views." <> dataTitle <> ".Show")
  -- new view
  let newViewPath = "src/" <> unpack appName <> "/Web/Views/" <> unpack dataTitle <> "/New.hs"
  putStrLn ("Creating " <> newViewPath)
  newViewTemplate <- getDataFileName "app-web-views-plural-new.hs.txt"
  generateHaskellFile newViewTemplate appName dataName datumName newViewPath
    >>= appendCode
          ("  H.form ! A.action \"/" <> encodeUtf8 dataNameLower <> "\" ! A.method \"POST\" $ do")
          (flip fmap parsedFields $ \field ->
            "    H.input ! A.type_ \""
              <> (if (toLower $ snd field) == "text" then "text" else "number")
              <> "\" ! A.name \""
              <> encodeUtf8 datumNameLower
              <> "Builder"
              <> (encodeUtf8 . toTitle $ fst field)
              <> "\""
          )
    >>= B.writeFile newViewPath
  addToCabalFile appName (appName <> ".Web.Views." <> dataTitle <> ".New")
  -- edit view
  let editViewPath = "src/" <> unpack appName <> "/Web/Views/" <> unpack dataTitle <> "/Edit.hs"
  putStrLn ("Creating " <> editViewPath)
  editViewTemplate <- getDataFileName "app-web-views-plural-edit.hs.txt"
  generateHaskellFile editViewTemplate appName dataName datumName editViewPath
    >>= prependCode
          "        H.input ! A.type_ \"submit\" ! A.name \"Save\""
          (flip fmap parsedFields $ \field ->
            let fieldName = encodeUtf8 . toTitle $ fst field
            in
              "        H.input ! A.name \""
              <> encodeUtf8 datumNameLower
              <> fieldName
              <> "\" ! A.value (fromString . "
              <> (if (toLower $ snd field) == "text" then "unpack" else "show")
              <> " $ "
              <> encodeUtf8 datumNameLower
              <> fieldName
              <> " "
              <> encodeUtf8 datumNameLower
              <> ")"
          )
    >>= B.writeFile editViewPath
  addToCabalFile appName (appName <> ".Web.Views." <> dataTitle <> ".Edit")
  -- add the appropriate schemas
  generate config conn appName (Right (Data (dataName : datumName : fields))) True
generate _ _ _ _ _ = do
  putStrLn "Feng Error: arguments to the generator seems to be off."
  putStrLn ""
  putStrLn "If the mistake isn't obvious, check out the help option for feng:"
  putStrLn "    $ feng -h"
  exitFailure

-- FIXME: this can easily cause problems
parseField :: Text -> (Text, Text)
parseField field =
  let
    first = fst $ breakOn ":" field
    second = snd . T.splitAt 1 . snd $ breakOn ":" field
  in (first, second)

-- | generates src/APP_NAME/Data/Resources.hs
genPluralResource :: Text -> Text -> Text -> [(Text, Text)] -> IO ()
genPluralResource appName pluralForm resource parsedFields = do
  let
    dataExporterPath =
      "src/" <> unpack appName <> "/Data/" <> (unpack $ toTitle pluralForm) <> ".hs"
  dataExporterTemplate <- getDataFileName "app-data-plural.hs.txt"
  generateHaskellFile dataExporterTemplate appName pluralForm resource dataExporterPath
  putStrLn ("Created " <> dataExporterPath)
  addToCabalFile appName (appName <> ".Data." <> toTitle pluralForm)
  putStrLn "Updated cabal file."

-- | generates src/APP_NAME/Data/RESOURCES/RESOURCE.hs
genSingularResource :: Text -> Text -> Text -> [(Text, Text)] -> Bool -> IO ()
genSingularResource appName pluralForm resource parsedFields includeView = do
  let
    dataDefinitionPath =
      "src/"
        <> unpack appName
        <> "/Data/"
        <> (unpack $ toTitle pluralForm)
        <> "/"
        <> (unpack $ toTitle resource)
        <> ".hs"
  dataDefinitionTemplate <- if includeView
    then getDataFileName "app-data-plural-singular-with-view.hs.txt"
    else getDataFileName "app-data-plural-singular.hs.txt"
  generateDataFileWithImports
    dataDefinitionTemplate
    appName
    pluralForm
    resource
    parsedFields
    dataDefinitionPath
  putStrLn ("Created " <> dataDefinitionPath)
  addToCabalFile appName (appName <> ".Data." <> toTitle pluralForm <> "." <> toTitle resource)
  putStrLn "Updated cabal file."

-- | adds db statements to src/APP_NAME/Data/RESOURCES/
genStatements :: Text -> Text -> Text -> [(Text, Text)] -> IO ()
genStatements appName dataName datumName fields = do
  let
    dataStatementsPath =
      "src/"
        <> unpack appName
        <> "/Data/"
        <> (unpack $ toTitle dataName)
        <> "/"
        <> (unpack $ toTitle datumName)
        <> "Statements.hs"
    imports =
      "import "
        <> (encodeUtf8 $ toTitle appName)
        <> ".Data."
        <> (encodeUtf8 $ toTitle dataName)
        <> "."
        <> (encodeUtf8 $ toTitle datumName)
        <> " ("
        <> (encodeUtf8 $ toTitle datumName)
        <> " (..), "
        <> (encodeUtf8 $ toTitle datumName)
        <> "Builder (..))"
  putStrLn ("Creating " <> dataStatementsPath)
  dataStatementsTemplate <- getDataFileName "app-data-plural-singularStatements.hs.txt"
  generateHaskellFile dataStatementsTemplate appName dataName datumName dataStatementsPath
    >>= appendCode "-- generator marker: internal imports" [imports]
    >>= addStatements dataName datumName fields
    >>= B.writeFile dataStatementsPath
  addToCabalFile
    appName
    (appName <> ".Data." <> toTitle dataName <> "." <> toTitle datumName <> "Statements")
 where
  addStatements :: Text -> Text -> [(Text, Text)] -> ByteString -> IO ByteString
  addStatements dataName datumName fields file = do
    let
      dataNameLower = encodeUtf8 $ toLower dataName
      datumNameTitle = encodeUtf8 $ toTitle datumName
      datumNameLower = encodeUtf8 $ toLower datumName
      stringInsertStatement =
        "  let q = \"INSERT INTO "
          <> dataNameLower
          <> " ("
          <> keys
          <> ") VALUES("
          <> questionMarks
          <> ")\""
      accessors = C.intercalate ", " . flip fmap fields $ \field ->
        let key = (encodeUtf8 . toTitle $ fst field)
        in datumNameLower <> "Builder" <> key <> " builder"
      clientInsertStatement =
        "  _rows_affected :: Int64 <- execute conn q (" <> accessors <> ")"
      stringUpdateStatement =
        "  let q = \"UPDATE "
          <> dataNameLower
          <> " SET("
          <> keys
          <> ") = ("
          <> questionMarks
          <> ") where id = ?\""
      clientUpdateValues =
        flip (++) [" " <> datumNameLower <> "Id " <> datumNameLower]
          $ flip fmap fields
          $ \field ->
              let fieldName = encodeUtf8 . toTitle $ fst field
              in datumNameLower <> fieldName <> " " <> datumNameLower
      clientUpdateStatementParams = "(" <> (C.intercalate ", " clientUpdateValues) <> ")"
      keys = encodeUtf8 . T.intercalate ", " $ fmap fst fields
      questionMarks = encodeUtf8 . T.intercalate "," $ fmap (const "?") [1 .. length fields]
    appendCode
        ("create" <> datumNameTitle <> " conn builder = do")
        [stringInsertStatement, clientInsertStatement]
        file
      >>= appendCode
            ("update" <> datumNameTitle <> " conn " <> datumNameLower <> " = do")
            [stringUpdateStatement, "  _result :: Int64 <- execute conn q " <> clientUpdateStatementParams]

-- | adds migration scripts to files/migrations
genMigrationScriptsAndLog :: Text -> Text -> Text -> [(Text, Text)] -> Feng.Config -> IO ()
genMigrationScriptsAndLog appName pluralForm resource parsedFields config = do
  dirExists <- doesDirectoryExist "files/migrations"
  createDirectoryIfMissing True "files/migrations"
  migrationTemplate <- getDataFileName "files-migrations-data.sql.txt"
  formattedTime <- fmap (formatTime defaultTimeLocale "%0Y%m%d%H%M%S") getCurrentTime
  generateSqlFile
    migrationTemplate
    appName
    pluralForm
    resource
    parsedFields
    ("files/migrations/" <> formattedTime <> "_add_" <> (unpack $ toLower pluralForm) <> ".sql")
  let dbName = Feng.dbName config
  putStrLn ""
  putStrLn "Remember to update your database by running migrations:"
  putStrLn "    $ feng migrate"
  putStrLn ""
