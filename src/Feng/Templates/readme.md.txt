# {{app_name_title}}

To hop into a development environment, run `nix-shell`. From there, you can
use all the usual cabal commands. Check out the `shell.nix` file to see what
other commands are available (Hlint, ghcid, etc.)

## Learn more

  * Docs: https://hackage.haskell.org/package/feng
  * Chat: https://gitter.im/fengframework/feng
  * Source: https://github.com/fengframework/feng
