{ pkgs ? import (builtins.fetchTarball {
  url = "https://github.com/NixOS/nixpkgs/archive/20.09.tar.gz";
  sha256 = "1wg61h4gndm3vcprdcg7rc4s1v3jkm5xd7lw8r2f67w502y94gcy";
  }) {}
}:

let
  inherit (pkgs) haskellPackages;

  haskellDeps = ps: with ps; [
    aeson
    base
    blaze-html
    blaze-markup
    hlint
    hspec
    mtl
    network
    postgresql-simple
    postgresql-simple-migration
    resource-pool
    servant
    servant-blaze
    servant-server
    text
    time
    wai
    wai-extra
    wai-logger
    warp
  ];
  ghc = haskellPackages.ghcWithPackages haskellDeps;

  nixPackages = [
    ghc
    haskellPackages.cabal-install
    haskellPackages.blaze-from-html
    haskellPackages.brittany
    haskellPackages.ghcid
    haskellPackages.hlint
    haskellPackages.stylish-haskell
    pkgs.postgresql
  ];

in

pkgs.stdenv.mkDerivation {
  name = "env";
  buildInputs = nixPackages;
  shellHook = ''
    export APP_NAME={{app_name_title}}
    export DB_NAME={{app_name_lower}}
    export DB_USER=dev
    export PGDATA=$PWD/.db/{{app_name_lower}}_data
    export PGHOST=$PWD/.db/$DB_NAME
    export LOG_PATH=$PWD/.db/$DB_NAME/LOG
    export DATABASE_URL="postgresql:///$DB_NAME?host=$PGHOST"
    export SUBPOOL_COUNT=5
    export RESOURCE_IDLE_TIME=0.5
    export MAX_SUBPOOL_RESOURCES=1
    export HTTP_PORT=8080

    if [ ! -d ".git" ]; then
      git init && \
      git add --all . && \
      git commit -m "project initialized" && \
      git branch -M main
    fi

    if [ ! -d $PGHOST ]; then
      mkdir -p $PGHOST
    fi

    if [ ! -d $PGDATA ]; then
      echo 'Initializing postgresql database...'
      initdb -U dev -D $PGDATA --no-locale --encoding=UTF8 --auth=trust >/dev/null && \
      pg_ctl -U dev -D $PGDATA -l $LOG_PATH -o "--unix_socket_directories='$PGHOST'" start && \
      createuser -U dev && \
      echo "Created database user \"dev\"" && \
      createdb -U dev -O dev {{app_name_lower}} && \
      migrate init "user=dev dbname={{app_name_lower}}"
      echo "Created database \"{{app_name_lower}}\" with owner \"dev\""
      echo ""
      echo "To get an interactive db session, run:"
      echo "    $ psql -U dev {{app_name_lower}}"
      echo ""
      echo "To see available feng commands, run:"
      echo "    $ feng --help"
      echo ""
      echo "You can build and start your app using cabal:"
      echo "    $ cabal build"
      echo "    $ cabal exec {{app_name_lower}}"
      echo ""
      echo "Time for take off!"
      echo ""
    fi

    function startdb {
      pg_ctl -D $PGDATA -l $LOG_PATH -o "--unix_socket_directories='$PGHOST'" start
    }

    function stopdb {
      pg_ctl -D $PGDATA -l $LOG_PATH -o "--unix_socket_directories='$PGHOST" stop
    }
  '';
}
