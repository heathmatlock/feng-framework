module {{app_name_title}}.Web.Handlers.{{data_name_title}}
  ( create{{datum_name_title}}
  , delete{{datum_name_title}}Html
  , delete{{datum_name_title}}Json
  , edit{{datum_name_title}}
  , list{{data_name_title}}
  , new{{datum_name_title}}
  , show{{datum_name_title}}
  , update{{datum_name_title}}Html
  , update{{datum_name_title}}Json
  )
where

import {{app_name_title}}.Data.{{data_name_title}}.{{datum_name_title}}
  ( {{datum_name_title}} (..)
  , {{datum_name_title}}Builder (..)
  , {{datum_name_title}}Editor (..)
  , default{{datum_name_title}}Builder
  )
import qualified {{app_name_title}}.Data.{{data_name_title}}.{{datum_name_title}}Statements as {{datum_name_title}}Statements
import Control.Monad.Except (throwError)
import Control.Monad.IO.Class (liftIO)
import Data.Pool (Pool, withResource)
import Database.PostgreSQL.Simple (Connection)
import Extra.Data.NoForm (NoForm)
import Servant.API (NoContent (NoContent))
import Servant.Server (Handler, err303, err404, errHeaders)

list{{data_name_title}} :: Pool Connection -> Handler [{{datum_name_title}}]
list{{data_name_title}} pool = withResource pool $ \conn -> liftIO $ {{datum_name_title}}Statements.list{{data_name_title}} conn

new{{datum_name_title}} :: Handler {{datum_name_title}}Builder
new{{datum_name_title}} = pure default{{datum_name_title}}Builder

create{{datum_name_title}} :: Pool Connection -> {{datum_name_title}}Builder -> Handler a
create{{datum_name_title}} pool builder = withResource pool $ \conn -> do
  liftIO $ {{datum_name_title}}Statements.create{{datum_name_title}} conn builder
  throwError err303 { errHeaders = [("Location", "/{{data_name_lower}}")] }

show{{datum_name_title}} :: Pool Connection -> Int -> Handler {{datum_name_title}}
show{{datum_name_title}} pool identifier = withResource pool $ \conn -> do
  r <- liftIO $ {{datum_name_title}}Statements.get{{datum_name_title}} conn identifier
  case r of
    Just {{data_name_lower}} -> pure {{data_name_lower}}
    Nothing -> throwError err404

delete{{datum_name_title}}Html :: Pool Connection -> Int -> NoForm -> Handler a
delete{{datum_name_title}}Html pool identifier _ = withResource pool $ \conn -> do
  liftIO $ {{datum_name_title}}Statements.delete{{datum_name_title}} conn identifier
  throwError err303 { errHeaders = [("Location", "/{{data_name_lower}}")] }

delete{{datum_name_title}}Json :: Pool Connection -> Int -> Handler NoContent
delete{{datum_name_title}}Json pool identifier = withResource pool $ \conn -> liftIO $ do
  {{datum_name_title}}Statements.delete{{datum_name_title}} conn identifier
  pure NoContent

update{{datum_name_title}}Html :: Pool Connection -> Int -> {{datum_name_title}} -> Handler a
update{{datum_name_title}}Html pool identifier {{data_name_lower}} = withResource pool $ \conn -> do
  _p <- liftIO $ {{datum_name_title}}Statements.update{{datum_name_title}} conn {{data_name_lower}}
  throwError err303 { errHeaders = [("Location", "/{{data_name_lower}}")] }

update{{datum_name_title}}Json :: Pool Connection -> Int -> {{datum_name_title}} -> Handler a
update{{datum_name_title}}Json pool _identifier {{data_name_lower}} = withResource pool $ \conn -> do
  _p <- liftIO $ {{datum_name_title}}Statements.update{{datum_name_title}} conn {{data_name_lower}}
  throwError err303 { errHeaders = [("Location", "/{{data_name_lower}}")] }

edit{{datum_name_title}} :: Pool Connection -> Int -> Handler {{datum_name_title}}Editor
edit{{datum_name_title}} pool identifier = withResource pool $ \conn -> do
  r <- liftIO $ {{datum_name_title}}Statements.get{{datum_name_title}} conn identifier
  case r of
    Nothing -> throwError err404
    Just {{data_name_lower}} -> pure $ {{datum_name_title}}Editor {{data_name_lower}}
