(function main() {
  handleJsDelete();
}) ();

function handleJsDelete () {
  const markers = document.querySelectorAll("a[data-method]");
  Array.from(markers).map(function(marker) {
    marker.addEventListener("click", function(e) {
      e.stopPropagation();
      e.preventDefault();
      if (e.target.dataset.method == "delete") {
        if (window.confirm("Are you sure you want to delete this?")) {
          deleteResource(e.target.href)
          .then((result) => {
            if (result.status === 200) {
              this.parentNode.parentNode.parentNode.removeChild(this.parentNode.parentNode);
            } else {
              // TODO: flash an error message
              return console.log("http status code isn't 200.", result);
            }
          })
          .catch((error) => {
            // TODO: flash an error message
            return console.log("error.", error);
          })
        } else {
          return alert("wutwut!");
        };
      } else {
        return alert("okey doke");
      };
    });
  });
};

async function deleteResource(url) {
  // Default options are marked with *
  return await fetch(url, {
    method: "DELETE",
    mode: "same-origin",
    cache: "no-cache",
    credentials: "omit",
    headers: {
      "Content-Type": "application/json"
    },
    redirect: "follow",
    referrerPolicy: "no-referrer",
  });
}
