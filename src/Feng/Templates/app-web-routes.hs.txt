{-# LANGUAGE DataKinds     #-}
{-# LANGUAGE TypeOperators #-}

module {{app_name_title}}.Web.Routes (Routes, routes) where

import Data.Proxy (Proxy (Proxy))
import Servant.API ((:<|>), (:>), Get, Raw)
import Servant.HTML.Blaze (HTML)

-- internal imports
import {{app_name_title}}.Data.Home (Home)

routes :: Proxy Routes
routes = Proxy

type Routes =
  HomeRoute
  :<|> StaticRoutes

type HomeRoute = Get '[HTML] Home

type StaticRoutes =
  "static"
  :> Raw
