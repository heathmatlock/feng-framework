{-# LANGUAGE ScopedTypeVariables #-}

module Feng.Prelude.File
  ( CaseStyle (..)
  , addRoutesToIndex
  , addToCabalFile
  , appendRouteHandlers
  , cabalFilename
  , generateDataFileWithImports
  , generateFile
  , generateHaskellFile
  , generateSqlFile
  , swap
  , appendCode
  , prependCode
  )
where

import Control.Exception (try)
import Data.ByteString (ByteString)
import qualified Data.ByteString as B
import qualified Data.ByteString.Char8 as C
import Data.List (findIndex, nub)
import Data.Text (Text, replace, toLower, toTitle, unpack)
import qualified Data.Text as T
import qualified Data.Text.Encoding as TE
import Feng.Prelude.Error (showRawErrorThenExit)
import System.Directory (createDirectoryIfMissing, doesFileExist)
import System.Directory.Internal.Prelude (isAlreadyExistsError)
import System.Exit (exitFailure)

data CaseStyle
  = Lower
  | Title
  deriving (Eq)

cabalFilename :: Text -> Text
cabalFilename appName = appName <> ".cabal"

generateFile :: FilePath -> Text -> FilePath -> IO ()
generateFile filePath appName destination = do
  possibleTemplate <- B.readFile filePath
  file <-
    swap "{{app_name_lower}}" appName Lower
      =<< swap "{{app_name_title}}" appName Title possibleTemplate
  B.writeFile destination file

generateDataFileWithImports :: FilePath
                            -> Text
                            -> Text
                            -> Text
                            -> [(Text, Text)]
                            -> FilePath
                            -> IO ()
generateDataFileWithImports filePath appName dataName datumName fields destination = do
  generateHaskellFile filePath appName dataName datumName destination
  let
    imports =
      "import "
        <> (TE.encodeUtf8 $ toTitle appName)
        <> ".Data."
        <> (TE.encodeUtf8 $ toTitle dataName)
        <> "."
        <> (TE.encodeUtf8 $ toTitle datumName)
        <> " ("
        <> (TE.encodeUtf8 $ toTitle datumName)
        <> " (..), "
        <> (TE.encodeUtf8 $ toTitle datumName)
        <> "Builder (..))"

  f <-
    appendCode "-- generator marker: internal imports" [imports]
    =<< addDefaultBuilderFunction datumName fields
    =<< addBuilder datumName fields
    =<< appendExternalImports fields
    =<< addDataDef destination dataName datumName fields
  B.writeFile destination f
 where
  appendExternalImports :: [(Text, Text)] -> ByteString -> IO ByteString
  appendExternalImports fields file = do
    let fileLines = C.lines file
    case findIndex (== "-- generator marker: external imports") fileLines of
      Nothing ->
        -- FIXME: possible failure here
        -- currently fails silently and passes along the bytestring
        pure file
      Just i -> do
        let
          split = splitAt (i + 1) fileLines
          newFile = (fst split) ++ newImports ++ (snd split)
          newImports = filter (not . C.null) (nub $ fmap genImport fields)
          -- FIXME: handle more types
          genImport :: (Text, Text) -> ByteString
          genImport t = case T.toLower (snd t) of
            "text" -> TE.encodeUtf8 "import Data.Text (Text)"
            "int" -> TE.encodeUtf8 ""
            _ ->
              TE.encodeUtf8
                "-- if your data needs anything but Text or Int for its fields, replace this line with the required import"
        pure $ C.unlines newFile

  addDataDef :: FilePath -> Text -> Text -> [(Text, Text)] -> IO ByteString
  addDataDef path dataName datumName fields = do
    file <- B.readFile path
    let
      fileLines = C.lines file
      utf8LowerDatum = TE.encodeUtf8 (toLower datumName)
    case findIndex (== "-- | data definition") fileLines of
      Nothing ->
        -- FIXME: possible failure here
        -- currently fails silently and passes along the bytestring
        pure file
      Just x -> do
        let
          split = splitAt (x + 1) fileLines
          utf8TitleDatum = TE.encodeUtf8 (toTitle datumName)
          newFile =
            (fst split)
              ++ ["data " <> utf8TitleDatum <> " = " <> utf8TitleDatum]
              ++ ["  { " <> utf8LowerDatum <> "Id :: Int"]
              ++ (concat
                   (fmap ((\x -> ["  , " <> head x]) . genCustomFields utf8LowerDatum) fields)
                 ) -- potential bug through use of head
              ++ ["  }"]
              ++ (snd split)
        pure $ C.unlines newFile

  addDefaultBuilderFunction :: Text -> [(Text, Text)] -> ByteString -> IO ByteString
  addDefaultBuilderFunction dataName fields file = do
    let fileLines = C.lines file
    case
        findIndex (== "-- | default builder definition used for building the new view") fileLines
      of
        Nothing ->
          -- FIXME: possible failure here
          -- currently fails silently and passes along the bytestring
          pure file
        Just x -> do
          let
            split = splitAt (x + 1) fileLines
            utf8TitleDatum = TE.encodeUtf8 (toTitle datumName)
            newFile =
              (fst split)
                ++ ([ B.concat
                        ([ "default"
                           <> utf8TitleDatum
                           <> "Builder = "
                           <> utf8TitleDatum
                           <> "Builder"
                         ]
                        ++ (fmap genVal (fmap snd fields))
                        )
                    ]
                   )
                ++ (snd split)
          pure $ C.unlines newFile

  genVal :: Text -> ByteString
  genVal val = case T.toLower val of
      -- FIXME: need to support more than just Text and Int :)
    "text" -> " \"\""
    _ -> " 0"

  addBuilder :: Text -> [(Text, Text)] -> ByteString -> IO ByteString
  addBuilder dataName fields file = do
    let fileLines = C.lines file
    case findIndex (== "-- | data builder definition") fileLines of
      Nothing ->
        -- FIXME: possible failure here
        -- currently fails silently and passes along the bytestring
        pure file
      Just x -> do
        let
          split = splitAt (x + 1) fileLines
          utf8TitleDatum = TE.encodeUtf8 (toTitle datumName)
          utf8LowerDatum = TE.encodeUtf8 (toLower datumName)
          fieldDefs = fmap (genCustomFields (utf8LowerDatum <> "Builder")) fields
          newFile =
            (fst split)
              ++ ["data " <> utf8TitleDatum <> "Builder = " <> utf8TitleDatum <> "Builder"]
              ++ ["  { " <> head (concat fieldDefs)] -- potential bug through use of head
              ++ (concat (fmap (\d -> ["  , " <> d]) (tail $ concat fieldDefs)))
              ++ ["  }"]
              ++ (snd split)
        pure $ C.unlines newFile


  genCustomFields :: ByteString -> (Text, Text) -> [ByteString]
  genCustomFields fieldPrefix (field, value) =
    -- TODO: let the user know if there's a typo or an unsupported value type
    [fieldPrefix <> (TE.encodeUtf8 $ toTitle field) <> " :: " <> (TE.encodeUtf8 $ toTitle value)]

generateSqlFile :: FilePath -> Text -> Text -> Text -> [(Text, Text)] -> FilePath -> IO ()
generateSqlFile filePath appName dataName datumName fields destination = do
  f <- addSql fields dataName =<< swap "{{data_name}}" dataName Lower =<< B.readFile filePath
  B.writeFile destination f
 where
  addSql :: [(Text, Text)] -> Text -> ByteString -> IO ByteString
  addSql fields dataName file = do
    let fileLines = C.lines file
    case findIndex (== "  -- custom fields") fileLines of
      Nothing -> showRawErrorThenExit
        "this shouldn't happen"
        "Cannot find the generator marker in sql file template."
      Just x -> do
        let
          split = splitAt (x + 1) fileLines
          newFile =
            (fst split)
              ++ (concat $ fmap (genSqlFromFields True) (init fields))
              ++ (concat $ fmap (genSqlFromFields False) ([last fields]))
              ++ (snd split)
        pure $ C.unlines newFile

  genSqlFromFields :: Bool -> (Text, Text) -> [ByteString]
  genSqlFromFields appendComma (field, value) = do
    let
      lineEnding = if appendComma then " not null," else " not null"
      lowerCaseVal = toLower value
      -- TODO What are the supported types?
      val = case lowerCaseVal of
        "int" -> "serial"
        "double" -> "float8"
        _ -> lowerCaseVal
    ["  " <> (TE.encodeUtf8 $ toLower field) <> " " <> (TE.encodeUtf8 val) <> lineEnding]

-- | Replaces variable placeholder in a template file for given Text
swap :: Text -> Text -> CaseStyle -> B.ByteString -> IO B.ByteString
swap placeholder newText caseStyle templatePath =
  let nText = if caseStyle == Title then toTitle newText else toLower newText
  in
    case TE.decodeUtf8' templatePath of
      Left err -> showRawErrorThenExit "an exception occured while reading in a template." err
      Right template ->
        let
          file = replace placeholder nText template
          fileUtf8 = TE.encodeUtf8 file
        in pure fileUtf8

addRoutesToIndex :: FilePath -> Text -> Text -> Text -> IO ()
addRoutesToIndex destination appName plural singular = do
  B.readFile destination
    >>= appendImport plural singular
    >>= prependCode
          "  :<|> StaticRoutes"
          ["  :<|> " <> (TE.encodeUtf8 $ toTitle singular) <> "Routes"]
    >>= B.writeFile destination
 where
  appendImport :: Text -> Text -> ByteString -> IO ByteString
  appendImport plural singular file = do
    let fileLines = C.lines file
    case findIndex (== "-- internal imports") fileLines of
      Nothing -> showRawErrorThenExit
        "import marker is missing in the routes index module."
        "Expected the comment \"-- internal imports\" to be in Web/Routes.hs"
      Just i -> do
        let
          split = splitAt (i + 1) fileLines
          newFile =
            (fst split)
              ++ [ "import "
                   <> (TE.encodeUtf8 $ toTitle appName)
                   <> ".Web.Routes."
                   <> (TE.encodeUtf8 $ toTitle plural)
                   <> " ("
                   <> (TE.encodeUtf8 $ toTitle singular)
                   <> "Routes)"
                 ]
              ++ (snd split)
        pure $ C.unlines newFile

appendRouteHandlers :: FilePath -> Text -> Text -> Text -> IO ()
appendRouteHandlers destination appName plural singular = do
  f <-
    appendHandlers plural singular
    =<< appendIndexHandler singular
    =<< appendImport plural singular
    =<< B.readFile destination
  B.writeFile destination f
 where
  appendImport :: Text -> Text -> ByteString -> IO ByteString
  appendImport plural singular file = do
    let fileLines = C.lines file
    case findIndex (== "-- generator marker: internal imports") fileLines of
      Nothing -> showRawErrorThenExit
        "this shouldn't happen"
        "Cannot find the imports generator marker in the Web.Handlers"
      Just i -> do
        let
          split = splitAt (i + 1) fileLines
          resourceTitlePlural = TE.encodeUtf8 $ toTitle plural
          resourceTitleSingular = TE.encodeUtf8 $ toTitle singular
          newFile =
            (fst split)
              ++ [ "import "
                   <> (TE.encodeUtf8 $ toTitle appName)
                   <> ".Web.Handlers."
                   <> resourceTitlePlural
                 ]
              ++ ["  ( create" <> resourceTitleSingular]
              ++ ["  , delete" <> resourceTitleSingular <> "Html"]
              ++ ["  , delete" <> resourceTitleSingular <> "Json"]
              ++ ["  , edit" <> resourceTitleSingular]
              ++ ["  , list" <> resourceTitlePlural]
              ++ ["  , new" <> resourceTitleSingular]
              ++ ["  , show" <> resourceTitleSingular]
              ++ ["  , update" <> resourceTitleSingular <> "Html"]
              ++ ["  , update" <> resourceTitleSingular <> "Json"]
              ++ ["  )"]
              ++ (snd split)
        pure $ C.unlines newFile

  appendIndexHandler :: Text -> ByteString -> IO ByteString
  appendIndexHandler singular file = do
    let fileLines = C.lines file
    case findIndex (== "  -- generator marker: index handler") fileLines of
      Nothing -> showRawErrorThenExit
        "this shouldn't happen"
        "Cannot find the index handler generator marker."
      Just i -> do
        let
          split = splitAt (i + 1) fileLines
          resourceTitleSingular = TE.encodeUtf8 $ toTitle singular
          newFile =
            (fst split)
              ++ ["    :<|> " <> (TE.encodeUtf8 $ toLower singular) <> "Handlers pool"]
              ++ (snd split)
        pure $ C.unlines newFile

  appendHandlers :: Text -> Text -> ByteString -> IO ByteString
  appendHandlers plural singular file = do
    let fileLines = C.lines file
    case findIndex (== "-- generator marker: handlers") fileLines of
      Nothing ->
        showRawErrorThenExit "this shouldn't happen" "Cannot find the handlers generator marker."
      Just i -> do
        let
          split = splitAt (i + 1) fileLines
          resourceTitlePlural = TE.encodeUtf8 $ toTitle plural
          resourceTitleSingular = TE.encodeUtf8 $ toTitle singular
          resourceLowerSingular = TE.encodeUtf8 $ toLower singular
          newFile =
            (fst split)
              ++ [""]
              ++ [resourceLowerSingular <> "Handlers pool ="]
              ++ ["  list" <> resourceTitlePlural <> " pool"]
              ++ ["    :<|> new" <> resourceTitleSingular]
              ++ ["    :<|> create" <> resourceTitleSingular <> " pool"]
              ++ ["    :<|> show" <> resourceTitleSingular <> " pool"]
              ++ ["    :<|> edit" <> resourceTitleSingular <> " pool"]
              ++ ["    :<|> update" <> resourceTitleSingular <> "Html pool"]
              ++ ["    :<|> update" <> resourceTitleSingular <> "Json pool"]
              ++ ["    :<|> delete" <> resourceTitleSingular <> "Html pool"]
              ++ ["    :<|> delete" <> resourceTitleSingular <> "Json pool"]
              ++ (snd split)
        pure $ C.unlines newFile

-- NOTE: since we're writing to a file and using this in a pipeline throughout
--       we're writing to disk twice. Need to refactor when things have settled.
generateHaskellFile :: FilePath -> Text -> Text -> Text -> FilePath -> IO ByteString
generateHaskellFile filePath appName dataName datumName destination = do
  file <-
    swap "{{app_name_title}}" appName Title
    =<< swap "{{datum_name_lower}}" datumName Lower
    =<< swap "{{datum_name_title}}" datumName Title
    =<< swap "{{data_name_title}}" dataName Title
    =<< swap "{{data_name_lower}}" dataName Lower
    =<< B.readFile filePath
  B.writeFile destination file
  pure file

deleteComment :: ByteString -> ByteString -> IO ByteString
deleteComment comment file = pure file

prependCode :: ByteString -> [ByteString] -> ByteString -> IO ByteString
prependCode marker code file = do
  let fileLines = C.lines file
  case findIndex (== marker) fileLines of
    Nothing -> do
      putStrLn "can't find marker: "
      print marker
      pure file
    Just i -> do
      let
        split = splitAt (i) fileLines
        newFile = (fst split) ++ code ++ (snd split)
      pure $ C.unlines newFile

appendCode :: ByteString -> [ByteString] -> ByteString -> IO ByteString
appendCode marker code file = do
  let fileLines = C.lines file
  case findIndex (== marker) fileLines of
    Nothing -> do
      putStrLn "can't find marker: "
      print marker
      pure file
    Just i -> do
      let
        split = splitAt (i + 1) fileLines
        newFile = (fst split) ++ code ++ (snd split)
      pure $ C.unlines newFile

addToCabalFile :: Text -> Text -> IO ()
addToCabalFile appName moduleName = do
  f <- addModule moduleName =<< B.readFile (unpack (appName <> ".cabal"))
  B.writeFile (unpack $ cabalFilename appName) f
 where
  addModule :: Text -> ByteString -> IO ByteString
  addModule moduleName' file = do
    let fileLines = C.lines file
    case findIndex (== "  other-modules:") fileLines of
      Nothing -> showRawErrorThenExit
        "this shouldn't happen"
        "Cannot find the generator marker in cabal file template."
      Just i -> do
        let
          split = splitAt (i + 1) fileLines
          newFile = (fst split) ++ ["    " <> TE.encodeUtf8 moduleName] ++ (snd split)
        pure $ C.unlines newFile
