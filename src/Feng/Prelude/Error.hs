module Feng.Prelude.Error where

import Data.Text (Text, unpack)
import System.Exit (exitFailure)

showRawErrorThenExit :: (Show e) => Text -> e -> IO a
showRawErrorThenExit explanation err = do
  putStrLn ("Feng Error: " <> unpack explanation)
  putStrLn ""
  putStrLn "Here's the raw error:"
  putStrLn (show err)
  putStrLn ""
  putStrLn "Feel free to report this at:"
  putStrLn "https://github.com/fengframework/feng/issues"
  exitFailure
