module Feng.Config
  ( Config(..)
  , buildConfigOrExit
  )
where

import qualified Data.ByteString as B
import Data.Text (Text, breakOnEnd, lines, pack, strip, toTitle, unpack)
import Data.Text.Encoding (decodeUtf8')
import Prelude hiding (lines)
import System.Environment (lookupEnv)
import System.Exit (exitFailure)

data Config = Config
  { appName :: Text
  ,  dbUser :: Text
  ,  dbName :: Text
  ,  subPoolCount :: Int
  ,  resourceIdleTime :: Rational
  ,  maxResourcesPerSubPool :: Int
  }

buildConfigOrExit :: IO Config
buildConfigOrExit = do
  app <- findEnvOrExit "APP_NAME"
  user <- findEnvOrExit "DB_USER"
  name <- findEnvOrExit "DB_NAME"
  count <- fmap unpack $ findEnvOrExit "SUBPOOL_COUNT"
  idleTime <- fmap unpack $ findEnvOrExit "RESOURCE_IDLE_TIME"
  maxResources <- fmap unpack $ findEnvOrExit "MAX_SUBPOOL_RESOURCES"
  pure $ Config
    { appName = app
    , dbUser = user
    , dbName = name
    , subPoolCount = read count :: Int
    , resourceIdleTime = toRational (read idleTime :: Double)
    , maxResourcesPerSubPool = read maxResources :: Int
    }

findEnvOrExit :: Text -> IO Text
findEnvOrExit envKey = do
  possibleVal <- lookupEnv (unpack envKey)
  case possibleVal of
    Nothing -> do
      putStrLn ("Feng: failed to find env var " <> unpack envKey)
      exitFailure
    Just "" -> do
      putStrLn ("Feng: env var " <> unpack envKey <> " is empty.")
      exitFailure
    Just v -> pure . strip . pack $ v
