{ pkgs ? import (builtins.fetchTarball {
  url = "https://github.com/NixOS/nixpkgs/archive/20.09.tar.gz";
  sha256 = "1wg61h4gndm3vcprdcg7rc4s1v3jkm5xd7lw8r2f67w502y94gcy";
  }) {}
}:

let
  inherit (pkgs) haskellPackages;

  haskellDeps = ps: with ps; [
    base
    bytestring
    directory
    postgresql-simple
    postgresql-simple-migration
    text
    time
  ];
  ghc = haskellPackages.ghcWithPackages haskellDeps;

  nixPackages = [
    ghc
    haskellPackages.cabal-install
    pkgs.ghcid
    pkgs.postgresql
  ];

in

pkgs.stdenv.mkDerivation {
  name = "env";
  buildInputs = nixPackages;
}
