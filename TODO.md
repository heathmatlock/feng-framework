# TODO

- [ ] add what you have
- [ ] make it public
- [ ] add form validations to blimpse
- [ ] add form validations to feng
- [ ] add csrf to blimpse
- [ ] add csrf to feng
- [ ] make the site accessible
- [ ] add turbolinks/morphdom/instant.page to help with performance
- [ ] get lighthouse scores to 100
- [ ] hot reload
- [ ] add created-at timestamp
- [ ] possibly add mail service
- [ ] clean up the mess
- [ ] add missing templates to feng
- [ ] show time when product was created? 
- [ ] make the site accessible
- [ ] get that lighthouse score to 100, then release :)
- [ ] add a test that tests some feature in the app
  - https://docs.servant.dev/en/stable/cookbook/testing/Testing.html
- [ ] add integration tests
- [ ] snapshot files before making changes and restore to original state if failure occurs

----------------------------------------------------------------
- [x] add DELETE confirmation modal
- [x] fix the issue of creating multiple cabal files
- [x] redirect the user upon manipulating a resource (like phoenix)
- [x] add edit route
- [x] add update routes (patch and put)
- [x] add delete route
----------------------------------------------------------------

### Features
- [x] Support additional routes (GET /resource?id=x, PUT, POST, etc.)
  - [x] forms
  - [ ] validations (maybe use phantom types)
    - [ ] max form input size verified at the edge
  - [ ] sessions
  - [ ] cookies
  - [ ] csrf by default (like phoenix)
- [ ] hot reload with morphdom and turbolinks
- [ ] path helpers?

### CI
- [ ] feng compiling as a nixpkg
- [ ] deploy with nix-ops
- [ ] add gitlab-ci.yml file

### Optimizations
- [ ] add optparse-applicative
- [ ] add envy
- [ ] maybe add hasql
- [ ] internationalization
- [ ] the "new" command should print in color what is being in created
- [ ] the "gen" command should print in color what is being in created
- [ ] probably a good idea to set prefixes in formField names
- [ ] cute cli output:
  ✨  Done in 7.19s.
  Green text 🎉 🍰
- migration header as in the migrations for rails:
$ cat files/migrations/users.sql
--  This file is auto-generated from the current state of the database. Instead
--  of editing this file, please use the migrations feature of Active Record to
--  incrementally modify your database, and then regenerate this schema definition.
-- 
--  This file is the source Rails uses to define your schema when running `rails
--  db:schema:load`. When creating a new database, `rails db:schema:load` tends to
--  be faster and is potentially less error prone than running all of your
--  migrations from scratch. Old migrations may fail to apply correctly if those
--  migrations use external dependencies or application code.
-- 
--  It's strongly recommended that you check this file into your version control system.

### Final Cutover to Public Release
- [ ] cli docs could be as good as mix and phoenix
- [ ] refactor before initial public availability
- [ ] feng.dev/docs

### Roadmap to Public Annoucement
- [ ] Channel abstraction
- [ ] haxl

### Additional Niceties
- [ ] servant-webdriver


### Routes supported by html generator

$ rails routes
               Prefix Verb   URI Pattern                  Controller#Action
  - [x]          root GET    /                            welcome#index
  - [x]      products GET    /products                    products#index
  - [x]               POST   /products                    products#create
  - [x]   new_product GET    /products/new                products#new
  - [X]  edit_product GET    /products/:id/edit           products#edit
  - [x]       product GET    /products/:id                products#show
  - [ ]               PATCH  /products/:id                products#update
  - [X]               PUT    /product/:id                 products#update
  - [x]               DELETE /products/:id                products#destroy

$ mix phx.routes
   page_path  GET     /                                   OogWeb.PageController :index
product_path  GET     /products                           OogWeb.ProductController :index
product_path  GET     /products/:id/edit                  OogWeb.ProductController :edit
product_path  GET     /products/new                       OogWeb.ProductController :new
product_path  GET     /products/:id                       OogWeb.ProductController :show
product_path  POST    /products                           OogWeb.ProductController :create
product_path  PATCH   /products/:id                       OogWeb.ProductController :update
              PUT     /products/:id                       OogWeb.ProductController :update
product_path  DELETE  /products/:id                       OogWeb.ProductController :delete
   websocket  WS      /socket/websocket                   OogWeb.UserSocket


### Settle issues Ed experienced:

- [x] fix missing pg_config issue
- [x] fix not having cabal for developers of feng
- [x] verbose log output for the warp server (use runSettings instead of run)

^ These seemed to have resolved themselves

