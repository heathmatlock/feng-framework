create table if not exists products (
  id serial primary key not null,
  -- custom fields
  name text not null,
  price serial not null
)
