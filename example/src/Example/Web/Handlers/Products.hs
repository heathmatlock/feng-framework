module Example.Web.Handlers.Products
  ( createProduct
  , deleteProductHtml
  , deleteProductJson
  , editProduct
  , listProducts
  , newProduct
  , showProduct
  , updateProductHtml
  , updateProductJson
  )
where

import Example.Data.Products.Product
  ( Product (..)
  , ProductBuilder (..)
  , ProductEditor (..)
  , defaultProductBuilder
  )
import qualified Example.Data.Products.ProductStatements as ProductStatements
import Control.Monad.Except (throwError)
import Control.Monad.IO.Class (liftIO)
import Data.Pool (Pool, withResource)
import Database.PostgreSQL.Simple (Connection)
import Extra.Data.NoForm (NoForm)
import Servant.API (NoContent (NoContent))
import Servant.Server (Handler, err303, err404, errHeaders)

listProducts :: Pool Connection -> Handler [Product]
listProducts pool = withResource pool $ \conn -> liftIO $ ProductStatements.listProducts conn

newProduct :: Handler ProductBuilder
newProduct = pure defaultProductBuilder

createProduct :: Pool Connection -> ProductBuilder -> Handler a
createProduct pool builder = withResource pool $ \conn -> do
  liftIO $ ProductStatements.createProduct conn builder
  throwError err303 { errHeaders = [("Location", "/products")] }

showProduct :: Pool Connection -> Int -> Handler Product
showProduct pool identifier = withResource pool $ \conn -> do
  r <- liftIO $ ProductStatements.getProduct conn identifier
  case r of
    Just products -> pure products
    Nothing -> throwError err404

deleteProductHtml :: Pool Connection -> Int -> NoForm -> Handler a
deleteProductHtml pool identifier _ = withResource pool $ \conn -> do
  liftIO $ ProductStatements.deleteProduct conn identifier
  throwError err303 { errHeaders = [("Location", "/products")] }

deleteProductJson :: Pool Connection -> Int -> Handler NoContent
deleteProductJson pool identifier = withResource pool $ \conn -> liftIO $ do
  ProductStatements.deleteProduct conn identifier
  pure NoContent

updateProductHtml :: Pool Connection -> Int -> Product -> Handler a
updateProductHtml pool identifier products = withResource pool $ \conn -> do
  _p <- liftIO $ ProductStatements.updateProduct conn products
  throwError err303 { errHeaders = [("Location", "/products")] }

updateProductJson :: Pool Connection -> Int -> Product -> Handler a
updateProductJson pool _identifier products = withResource pool $ \conn -> do
  _p <- liftIO $ ProductStatements.updateProduct conn products
  throwError err303 { errHeaders = [("Location", "/products")] }

editProduct :: Pool Connection -> Int -> Handler ProductEditor
editProduct pool identifier = withResource pool $ \conn -> do
  r <- liftIO $ ProductStatements.getProduct conn identifier
  case r of
    Nothing -> throwError err404
    Just products -> pure $ ProductEditor products
