module Example.Web.Handlers.Home (home) where

import Example.Data.Home (Home, index)
import Servant (Handler)

home :: Handler Home
home = pure index
