module Example.Web.Views.Products.New where

import Text.Blaze.Html5 as H (Markup, ToMarkup, (!))
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A

import Example.Data.Products.Product (ProductBuilder (..))
import qualified Example.Web.Layouts.Main as MainLayout

instance ToMarkup ProductBuilder where
  toMarkup builder = render builder

render :: ProductBuilder -> Markup
render _defaultBuilder = MainLayout.render $ H.div ! A.class_ "create-product" $ do
  H.form ! A.action "/products" ! A.method "POST" $ do
    H.input ! A.type_ "text" ! A.name "productBuilderName"
    H.input ! A.type_ "number" ! A.name "productBuilderPrice"
    H.input ! A.type_ "submit" ! A.name "productBuilderSubmit"
