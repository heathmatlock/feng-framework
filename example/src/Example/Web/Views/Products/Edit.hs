module Example.Web.Views.Products.Edit where

import Data.String (fromString)
import Data.Text (pack, unpack)
import Text.Blaze.Html5 as H (Markup, ToMarkup, toMarkup, (!))
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A

import Example.Data.Products.Product (Product (..), ProductEditor (..))
import qualified Example.Web.Layouts.Main as MainLayout

instance ToMarkup ProductEditor where
  toMarkup a = render a

render :: ProductEditor -> Markup
render (ProductEditor product) = MainLayout.render $ do
  H.h1 $ H.text "Edit Product"
  H.form
    ! (A.action ("/products/" <> (fromString . show $ productId product) <> "/edit"))
    ! A.method "POST"
    $ do
        H.input ! A.type_ "hidden" ! A.name "productId" ! A.value
          (fromString . show $ productId product)
        H.input ! A.name "productName" ! A.value (fromString . unpack $ productName product)
        H.input ! A.name "productPrice" ! A.value (fromString . show $ productPrice product)
        H.input ! A.type_ "submit" ! A.name "Save"
