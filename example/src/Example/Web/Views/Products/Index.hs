{-# LANGUAGE FlexibleInstances #-}

module Example.Web.Views.Products.Index where
import Data.String (fromString)
import Text.Blaze.Html5 (Html, Markup, ToMarkup, toMarkup)
import Text.Blaze.Html5 (dataAttribute, (!))
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A

import Example.Data.Products.Product (Product (..))
import qualified Example.Web.Layouts.Main as MainLayout
import Example.Web.Views.Products.Show hiding (render)

instance ToMarkup [Product] where
  toMarkup products = render products

render :: [Product] -> Markup
render products = MainLayout.render $ do
  H.a ! A.href "/products/new" $ "+ New"
  H.table ! A.class_ "table" $ do
    H.thead $ H.tr $ do
      H.th "Products"
      H.th mempty -- resource column
      H.th mempty -- edit column
      H.th mempty -- delete column
    H.tbody $ do
      flip mapM_ products buildProductRow

buildProductRow :: Product -> Html
buildProductRow product = H.tr $ do
  H.td $ H.a ! A.href (fromString $ "/products/" <> (show $ productId product)) $ H.text
    (productName product)
  H.td $ H.a ! A.href (fromString $ "/products/" <> (show $ productId product) <> "/edit") $ "Edit"
  H.td
    $ H.a
    ! A.href (fromString $ "/products/" <> (show $ productId product))
    ! dataAttribute "method" "delete"
    $ "Delete"
