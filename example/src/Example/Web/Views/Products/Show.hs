module Example.Web.Views.Products.Show where

import Example.Data.Products.Product (Product (..))
import qualified Example.Web.Layouts.Main as MainLayout
import Data.Text (pack)
import Text.Blaze.Html5 (Markup, ToMarkup, toMarkup)
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A

instance ToMarkup Product where
  toMarkup product = render product

render :: Product -> Markup
render product = MainLayout.render $ do
  H.h1 . H.text . pack . show $ productId product
