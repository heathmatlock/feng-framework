module Example.Web.Views.Home where

import qualified Example.Web.Layouts.Main as MainLayout
import Text.Blaze.Html5 (Markup, (!))
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A

render :: Markup
render = MainLayout.render $ H.h1 ! A.class_ "title" $ "Example has lift off!"
