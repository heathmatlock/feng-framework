module Example.Web.Layouts.Main where

import Text.Blaze ((!))
import Text.Blaze.Html5 (Markup)
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A

render :: Markup -> Markup
render page = H.docTypeHtml $ do
  H.head metadata
  H.body (body page)

metadata :: Markup
metadata = do
  H.title "Example · Taking Flight"
  H.link ! A.rel "stylesheet" ! A.href "/static/css/bulma.min.css"

body :: Markup -> Markup
body page = H.div ! A.class_ "container" $ page <> scripts

scripts = do
  H.script ! A.src "/static/js/feng.js" $ mempty
