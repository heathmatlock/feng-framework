{-# LANGUAGE DataKinds     #-}
{-# LANGUAGE TypeOperators #-}

module Example.Web.Routes (Routes, routes) where

import Data.Proxy (Proxy (Proxy))
import Servant.API ((:<|>), (:>), Get, Raw)
import Servant.HTML.Blaze (HTML)

-- internal imports
import Example.Web.Routes.Products (ProductRoutes)
import Example.Data.Home (Home)

routes :: Proxy Routes
routes = Proxy

type Routes =
  HomeRoute
  :<|> ProductRoutes
  :<|> StaticRoutes

type HomeRoute = Get '[HTML] Home

type StaticRoutes =
  "static"
  :> Raw
