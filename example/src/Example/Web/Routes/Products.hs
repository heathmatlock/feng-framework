{-# LANGUAGE DataKinds     #-}
{-# LANGUAGE TypeOperators #-}

module Example.Web.Routes.Products (ProductRoutes) where

import Servant.API
  ( (:<|>)
  , (:>)
  , Capture
  , Delete
  , FormUrlEncoded
  , Get
  , JSON
  , NoContent
  , Post
  , Put
  , ReqBody
  )
import Servant.HTML.Blaze (HTML)

import Extra.Data.NoForm (NoForm)
import Example.Data.Products (Product, ProductBuilder, ProductEditor)
--------------------------------------------------------
-- These View imports expose required ToMarkup instances
--------------------------------------------------------
import Example.Web.Views.Products.Edit
import Example.Web.Views.Products.Index
import Example.Web.Views.Products.New
---------------------------------------------------

type ProductRoutes
  = ListProducts
  :<|> NewProduct
  :<|> CreateProduct
  :<|> ShowProduct
  :<|> EditProduct
  :<|> UpdateProductHTML
  :<|> UpdateProductJSON
  :<|> DeleteProductHTML
  :<|> DeleteProductJSON

type ListProducts =
  "products"
  :> Get '[HTML, JSON] [Product]

type NewProduct =
  "products"
  :> "new"
  :> Get '[HTML] ProductBuilder

type CreateProduct =
  "products"
  :> ReqBody '[FormUrlEncoded, JSON] ProductBuilder
  :> Post '[HTML,JSON] NoContent

type ShowProduct =
  "products"
  :> Capture "id" Int
  :> Get '[HTML,JSON] Product

type EditProduct =
  "products"
  :> Capture "id" Int
  :> "edit"
  :> Get '[HTML,JSON] ProductEditor

type UpdateProductHTML =
  "products"
  :> Capture "id" Int
  :> "edit"
  :> ReqBody '[FormUrlEncoded] Product
  :> Post '[HTML] NoContent

type UpdateProductJSON =
  "products"
  :> Capture "id" Int
  :> ReqBody '[JSON] Product
  :> Put '[JSON] NoContent

type DeleteProductHTML =
  "products"
  :> Capture "id" Int
  :> "delete"
  :> ReqBody '[FormUrlEncoded] NoForm
  :> Post '[HTML] NoContent

type DeleteProductJSON =
  "products"
  :> Capture "id" Int
  :> Delete '[JSON] NoContent
