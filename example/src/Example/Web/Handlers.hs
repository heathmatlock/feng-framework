module Example.Web.Handlers (handlers) where

import Data.Pool (Pool)
import Database.PostgreSQL.Simple (Connection)
import Network.Wai (Application)
import Servant.API
import Servant.Server (Handler, Server, Tagged)
import Servant.Server.StaticFiles (serveDirectoryWebApp)

-- generator marker: internal imports
import Example.Web.Handlers.Products
  ( createProduct
  , deleteProductHtml
  , deleteProductJson
  , editProduct
  , listProducts
  , newProduct
  , showProduct
  , updateProductHtml
  , updateProductJson
  )
import Example.Web.Handlers.Home (home)
import Example.Web.Routes (Routes)

handlers :: Pool Connection -> Server Routes
handlers pool =
  home
  -- generator marker: index handler
    :<|> productHandlers pool
    :<|> handleRawRequest

-- generator marker: handlers

productHandlers pool =
  listProducts pool
    :<|> newProduct
    :<|> createProduct pool
    :<|> showProduct pool
    :<|> editProduct pool
    :<|> updateProductHtml pool
    :<|> updateProductJson pool
    :<|> deleteProductHtml pool
    :<|> deleteProductJson pool

handleRawRequest :: Tagged Handler Application
handleRawRequest = serveDirectoryWebApp "files/assets"
