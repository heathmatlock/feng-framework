module Example.Config (Config(..), buildConfigOrExit) where

import Data.Fixed (Pico)
import Data.Text (Text, pack, strip, unpack)
import System.Environment (lookupEnv)
import System.Exit (exitFailure)

data Config = Config
  { dbUser                 :: String
  , dbName                 :: String
  , subPoolCount           :: Int
  , resourceIdleTime       :: Rational
  , maxResourcesPerSubPool :: Int
  , httpServerPort         :: Int
  }

buildConfigOrExit :: IO Config
buildConfigOrExit = do
  user <- findEnvOrExit "DB_USER"
  name <- findEnvOrExit "DB_NAME"
  count <- findEnvOrExit "SUBPOOL_COUNT"
  idleTime <- findEnvOrExit "RESOURCE_IDLE_TIME"
  maxResources <- findEnvOrExit "MAX_SUBPOOL_RESOURCES"
  serverPort <- findEnvOrExit "HTTP_PORT"
  pure $ Config
    { dbUser = user
    , dbName = name
    , subPoolCount = read count :: Int
    , resourceIdleTime = toRational (read idleTime :: Double)
    , maxResourcesPerSubPool = read maxResources :: Int
    , httpServerPort = read serverPort :: Int
    }

findEnvOrExit :: Text -> IO String
findEnvOrExit envKey = do
  possibleVal <- lookupEnv (unpack envKey)
  case possibleVal of
    Nothing -> do
      putStrLn ("failed to find env var: " <> unpack envKey)
      exitFailure
    Just "" -> do
      putStrLn ("failed to find env var: " <> unpack envKey)
      exitFailure
    Just v -> pure . unpack . strip . pack $ v
