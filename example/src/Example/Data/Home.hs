module Example.Data.Home where

import qualified Example.Web.Views.Home as Home
import Text.Blaze.Html (ToMarkup, toMarkup)

newtype Home = Home ()

instance ToMarkup Home where
  toMarkup _unit = Home.render

index :: Home
index = Home ()
