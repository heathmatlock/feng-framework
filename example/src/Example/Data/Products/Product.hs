{-# LANGUAGE DeriveGeneric #-}

module Example.Data.Products.Product (
  Product(..),
  ProductBuilder(..),
  ProductEditor(..),
  defaultProductBuilder
) where

-- generator marker: external imports
import Data.Text (Text)
import Data.Aeson (FromJSON, ToJSON)
import Database.PostgreSQL.Simple.FromRow (FromRow)
import GHC.Generics (Generic)
import Web.FormUrlEncoded (FromForm, ToForm)

-----------------------------------------------

-- | data definition
data Product = Product
  { productId :: Int
  , productName :: Text
  , productPrice :: Int
  }
  deriving (Eq, Generic, Show)

instance FromForm Product
instance FromJSON Product
instance FromRow Product
instance ToJSON Product

-----------------------------------------------

-- | data builder definition
data ProductBuilder = ProductBuilder
  { productBuilderName :: Text
  , productBuilderPrice :: Int
  }
  deriving (Eq, Generic, Show)

instance FromJSON ProductBuilder
instance FromForm ProductBuilder

-- | default builder definition used for building the new view
defaultProductBuilder = ProductBuilder "" 0

-----------------------------------------------

newtype ProductEditor = ProductEditor Product
  deriving Generic

instance ToJSON ProductEditor
