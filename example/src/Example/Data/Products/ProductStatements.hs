{-# LANGUAGE ScopedTypeVariables #-}

module Example.Data.Products.ProductStatements (createProduct, deleteProduct, getProduct, listProducts, updateProduct) where

import Data.Int (Int64)
import Data.Maybe (listToMaybe)
import Data.String (fromString)
import Data.Text (Text)
import Database.PostgreSQL.Simple

-- generator marker: internal imports
import Example.Data.Products.Product (Product (..), ProductBuilder (..))

listProducts :: Connection -> IO [Product]
listProducts conn = query_ conn "SELECT * FROM products;"

createProduct :: Connection -> ProductBuilder -> IO ()
createProduct conn builder = do
  let q = "INSERT INTO products (name, price) VALUES(?,?)"
  _rows_affected :: Int64 <- execute conn q (productBuilderName builder, productBuilderPrice builder)
  pure ()

getProduct :: Connection -> Int -> IO (Maybe Product)
getProduct conn identifier = do
  result :: [Product] <- query conn "SELECT * FROM products WHERE id = ?" (Only (show identifier) :: Only String)
  pure (listToMaybe result)

deleteProduct :: Connection -> Int -> IO ()
deleteProduct conn identifier = do
  let q = "DELETE FROM products WHERE id = ?"
  _results :: Int64 <- execute conn q (Only (show identifier) :: Only String)
  pure ()

updateProduct :: Connection -> Product -> IO ()
updateProduct conn product = do
  let q = "UPDATE products SET(name, price) = (?,?) where id = ?"
  _result :: Int64 <- execute conn q (productName product, productPrice product,  productId product)
  pure ()
