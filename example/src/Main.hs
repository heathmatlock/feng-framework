{-# LANGUAGE TypeApplications #-}

module Main where

import Example.Config (Config (..), buildConfigOrExit)
import Example.Web.Handlers (handlers)
import Example.Web.Routes (Routes)
import Data.Pool (createPool)
import Database.PostgreSQL.Simple (close, connect, connectDatabase, connectUser, defaultConnectInfo)
import Network.Socket hiding (close, connect)
import Network.Wai.Handler.Warp (defaultSettings, runSettings, setLogger, setPort)
import Network.Wai.Logger (withStdoutLogger)
import Network.Wai.Middleware.Gzip (GzipFiles (GzipCompress), def, gzip, gzipFiles)
import Servant

main :: IO ()
main = do
  config <- buildConfigOrExit
  let
    port = httpServerPort config
    connectDB =
      connect defaultConnectInfo { connectUser = dbUser config, connectDatabase = dbName config }
  pool <- createPool
    connectDB
    close
    (subPoolCount config)
    (fromRational $ resourceIdleTime config)
    (maxResourcesPerSubPool config)
  withStdoutLogger $ \logger -> do
    let settings = setPort 8080 $ setLogger logger defaultSettings
    putStrLn ("App server listening on port " <> show port)
    putStrLn ("This process can be stopped manually with Ctrl-C")
    runSettings settings . id $ compress (app pool)
 where
  compress = gzip def { gzipFiles = GzipCompress }
  app pool = serve (Proxy @Routes) (handlers pool)
