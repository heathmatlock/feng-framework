module Extra.Data.NoForm where

import Web.FormUrlEncoded (FromForm (fromForm))

data NoForm = NoForm

instance FromForm NoForm where
  fromForm _a = Right NoForm
